module code.gitea.io/pkg/util

go 1.16

require (
	github.com/stretchr/testify v1.7.0
	github.com/unknwon/com v1.0.1
)
